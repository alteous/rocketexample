##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=basic
ConfigurationName      :=Debug
WorkspacePath          := "/home/alteous/rocketexample"
ProjectPath            := "/home/alteous/rocketexample/basic"
IntermediateDirectory  :=./Debug
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=David
Date                   :=24/11/15
CodeLitePath           :="/home/alteous/.codelite"
LinkerName             :=/usr/bin/g++
SharedObjectLinkerName :=/usr/bin/g++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="basic.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). $(IncludeSwitch)include 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)z $(LibrarySwitch)png $(LibrarySwitch)GL $(LibrarySwitch)GLEW $(LibrarySwitch)SDL2 $(LibrarySwitch)SDL2_image $(LibrarySwitch)RocketCore $(LibrarySwitch)RocketControls $(LibrarySwitch)RocketDebugger 
ArLibs                 :=  "z" "png" "GL" "GLEW" "SDL2" "SDL2_image" "RocketCore" "RocketControls" "RocketDebugger" 
LibPath                := $(LibraryPathSwitch). $(LibraryPathSwitch). $(LibraryPathSwitch)/usr/local/lib/x86_64-linux-gnu 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := /usr/bin/ar rcu
CXX      := /usr/bin/g++
CC       := /usr/bin/gcc
CXXFLAGS :=  -g -O0 -Wall $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := /usr/bin/as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=$(IntermediateDirectory)/src_main.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_RenderInterfaceSDL2.cpp$(ObjectSuffix) $(IntermediateDirectory)/src_SystemInterfaceSDL2.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

MakeIntermediateDirs:
	@test -d ./Debug || $(MakeDirCommand) ./Debug


$(IntermediateDirectory)/.d:
	@test -d ./Debug || $(MakeDirCommand) ./Debug

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/src_main.cpp$(ObjectSuffix): src/main.cpp $(IntermediateDirectory)/src_main.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/alteous/rocketexample/basic/src/main.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_main.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_main.cpp$(DependSuffix): src/main.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_main.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_main.cpp$(DependSuffix) -MM "src/main.cpp"

$(IntermediateDirectory)/src_main.cpp$(PreprocessSuffix): src/main.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_main.cpp$(PreprocessSuffix) "src/main.cpp"

$(IntermediateDirectory)/src_RenderInterfaceSDL2.cpp$(ObjectSuffix): src/RenderInterfaceSDL2.cpp $(IntermediateDirectory)/src_RenderInterfaceSDL2.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/alteous/rocketexample/basic/src/RenderInterfaceSDL2.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_RenderInterfaceSDL2.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_RenderInterfaceSDL2.cpp$(DependSuffix): src/RenderInterfaceSDL2.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_RenderInterfaceSDL2.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_RenderInterfaceSDL2.cpp$(DependSuffix) -MM "src/RenderInterfaceSDL2.cpp"

$(IntermediateDirectory)/src_RenderInterfaceSDL2.cpp$(PreprocessSuffix): src/RenderInterfaceSDL2.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_RenderInterfaceSDL2.cpp$(PreprocessSuffix) "src/RenderInterfaceSDL2.cpp"

$(IntermediateDirectory)/src_SystemInterfaceSDL2.cpp$(ObjectSuffix): src/SystemInterfaceSDL2.cpp $(IntermediateDirectory)/src_SystemInterfaceSDL2.cpp$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/alteous/rocketexample/basic/src/SystemInterfaceSDL2.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/src_SystemInterfaceSDL2.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/src_SystemInterfaceSDL2.cpp$(DependSuffix): src/SystemInterfaceSDL2.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/src_SystemInterfaceSDL2.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/src_SystemInterfaceSDL2.cpp$(DependSuffix) -MM "src/SystemInterfaceSDL2.cpp"

$(IntermediateDirectory)/src_SystemInterfaceSDL2.cpp$(PreprocessSuffix): src/SystemInterfaceSDL2.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/src_SystemInterfaceSDL2.cpp$(PreprocessSuffix) "src/SystemInterfaceSDL2.cpp"


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r ./Debug/


