.PHONY: clean All

All:
	@echo "----------Building project:[ basic - Debug ]----------"
	@cd "basic" && "$(MAKE)" -f  "basic.mk"
clean:
	@echo "----------Cleaning project:[ basic - Debug ]----------"
	@cd "basic" && "$(MAKE)" -f  "basic.mk" clean
